#!/bin/bash

snap refresh
snap install atom #--classic
snap install slack #--classic
snap install authy --beta
snap install mattermost
snap install nextcloud
snap install panwriter
snap refresh
