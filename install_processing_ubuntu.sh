#!/bin/bash

#Récupération de l'archive :
wget http://download.processing.org/processing-3.5.3-linux64.tgz && tar xvf processing-*.tgz && mkdir /usr/share/processing && mv processing-* /usr/share/processing

# récupération de l'image logo :
wget http://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Processing_Logo_Clipped.svg/200px-Processing_Logo_Clipped.svg.png
mv 200px* /usr/share/processing/processing.png
 
#création du fichier .desktop :
echo '[Desktop Entry]
Type=Application
Name=Processing
GenericName=Processing
Comment=Un environnement de développement pour le langage Processing
Icon=/usr/share/processing/processing.png
Exec=/usr/share/processing/processing-3.5.3/processing #Attention version
Terminal=false
StartupNotify=false
Categories=Development;Electronics' > processing.desktop
mv processing.desktop /usr/share/applications/ # puis on déplace le fichier