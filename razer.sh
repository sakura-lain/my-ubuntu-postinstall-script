#!/bin/bash

apt install software-properties-gtk
add-apt-repository ppa:openrazer/stable
apt install openrazer-meta
echo 'deb http://download.opensuse.org/repositories/hardware:/razer/xUbuntu_20.04/ /' | tee /etc/apt/sources.list.d/hardware:razer.list
curl -fsSL https://download.opensuse.org/repositories/hardware:razer/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/hardware:razer.gpg > /dev/null
apt install razergenie

