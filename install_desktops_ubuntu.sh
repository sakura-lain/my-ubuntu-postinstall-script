#!/bin/bash

#Cinnamon
apt install cinnamon

#Setting up terminator as the default terminal:
gsettings set org.gnome.desktop.default-applications.terminal exec /usr/bin/terminator
gsettings set org.gnome.desktop.default-applications.terminal exec-arg "-x"
gsettings set org.cinnamon.desktop.default-applications.terminal exec /usr/bin/terminator

#KDE

apt install -y kde-plasma-desktop
#apt install -y dolphin konqueror