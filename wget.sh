!#/bin/bash

#wget https://download3.vmware.com/software/player/file/VMware-Player-15.1.0-13591040.x86_64.bundle && chmod +x VMware-Player-15.1.0-13591040.x86_64.bundle && ./VMware-Player-15.1.0-13591040.x86_64.bundle
wget https://download3.vmware.com/software/player/file/VMware-Player-15.5.5-16285975.x86_64.bundle && chmod +x VMware-Player-15.5.5-16285975.x86_64.bundle && ./VMware-Player-15.5.5-16285975.x86_64.bundle

#Synology
#wget https://global.download.synology.com/download/Tools/CloudStationDrive/4.3.1-4437/Ubuntu/Installer/x86_64/synology-cloud-station-drive-4437.x86_64.deb && dpkg --install synology-cloud-station-drive-4437.x86_64.deb
wget https://global.download.synology.com/download/Tools/CloudStationDrive/4.3.3-4469/Ubuntu/Installer/x86_64/synology-cloud-station-drive-4469.x86_64.deb #&& dpkg --install synology-cloud-station-drive-4469.x86_64.deb
#wget https://global.download.synology.com/download/Tools/Assistant/6.2-23733/Ubuntu/x86_64/synology-assistant_6.2-23733_amd64.deb && dpkg -i synology-assistant_6.2-23733_amd64.deb
wget https://global.download.synology.com/download/Tools/Assistant/6.2-24922/Ubuntu/x86_64/synology-assistant_6.2-24922_amd64.deb #&& dpkg -i synology-assistant_6.2-24922_amd64.deb

wget https://download.xnview.com/XnViewMP-linux-x64.deb #&& dpkg -i XnViewMP-linux-x64.deb

#Arduino
#wget https://downloads.arduino.cc/arduino-1.8.9-linux64.tar.xz && tar xvf arduino-1.8.9-linux64.tar.xz && ./arduino-1.8.9/install.sh*
wget https://downloads.arduino.cc/arduino-1.8.12-linux64.tar.xz && tar xvf arduino-1.8.12-linux64.tar.xz && ./arduino-1.8.12/install.sh*
wget http://s4a.cat/downloads/S4A16.deb #&& dpkg -i S4A16.deb #Scratch for Arduino

wget http://media.steampowered.com/client/installer/steam.deb #&& dpkg -i steam.deb

#Contrôle à distance
#wget https://dl.teamviewer.com/download/linux/version_14x/teamviewer_14.3.4730_amd64.deb && dpkg -i teamviewer_14.3.4730_amd64.deb
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb #&& dpkg -i teamviewer_amd64.deb
#wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.19.325-Linux-x64.deb && dpkg -i VNC-Viewer-6.19.325-Linux-x64.deb
wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.20.529-Linux-x64.deb #&& dpkg -i VNC-Viewer-6.20.529-Linux-x64.deb

#wget https://code-industry.net/public/master-pdf-editor-5.4.33-qt5.amd64.deb && dpkg -i master-pdf-editor-5.4.33-qt5.amd64.deb
wget https://code-industry.net/public/master-pdf-editor-5.4.38-qt5-all.amd64.deb #&& dpkg -i master-pdf-editor-5.4.38-qt5-all.amd64.deb

wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sh /dev/stdin
