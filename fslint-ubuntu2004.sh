#!/bin/bash
#https://forum.ubuntu-fr.org/viewtopic.php?id=2051628

wget http://archive.ubuntu.com/ubuntu/pool/universe/p/pygtk/python-gtk2_2.24.0-6_amd64.deb
sudo dpkg -i python-gtk2_2.24.0-6_amd64.deb
sudo apt install -f
wget http://archive.ubuntu.com/ubuntu/pool/universe/p/pygtk/python-glade2_2.24.0-6_amd64.deb
sudo dpkg -i python-glade2_2.24.0-6_amd64.deb
wget http://archive.ubuntu.com/ubuntu/pool/universe/f/fslint/fslint_2.46-1_all.deb
sudo dpkg -i fslint_2.46-1_all.deb
