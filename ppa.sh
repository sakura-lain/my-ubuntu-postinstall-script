#!/bin/bash

echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee /etc/apt/sources.list.d/virtualbox.list && wget -q -O- http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc | apt-key add -
#echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee /etc/apt/sources.list.d/virtualbox.list
#wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add - #Typora
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAE #Typora
add-apt-repository -y ppa:mkusb/ppa
add-apt-repository -y ppa:kritalime/ppa
add-apt-repository -y ppa:ehbello/fritzing
add-apt-repository -y ppa:openscad/releases
add-apt-repository -y ppa:makehuman-official/makehuman-11x
add-apt-repository -y ppa:freecad-maintainers/freecad-stable
add-apt-repository -y ppa:vincent-vandevyvre/vvv #QArte 
add-apt-repository -y 'deb http://typora.io ./' #Typora pour Ubuntu
echo -e "\ndeb https://typora.io/linux ./" | sudo tee -a /etc/apt/sources.list #Typora pour Mint
add-apt-repository -y ppa:lutris-team/lutris
dpkg --add-architecture i386 #Wine
wget -nc https://dl.winehq.org/wine-builds/winehq.key #Wine
apt-key add winehq.key #Wine
apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/' #wine
add-apt-repository -y ppa:openrazer/stable  #Razer
add-apt-repository -y ppa:polychromatic/stable #Razer
add-apt-repository -y ppa:minetestdevs/stable
sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
add-apt-repository ppa:linuxuprising/shutter
#Celestia
wget -O- https://bintray.com/user/downloadSubjectPublicKey\?username\=bintray | apt-key add -
echo "deb https://dl.bintray.com/celestia/releases-deb bionic universe" | tee -a /etc/apt/sources.list
add-apt-repository ppa:stellarium/stellarium-releases

apt update
apt upgrade -y

# Après passage à LTS 20.04
# apt install -y virtualbox freecad shutter

