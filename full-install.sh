#!/bin/bash

#exec &>> install_ubuntu.log

#echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee /etc/apt/sources.list.d/virtualbox.list && wget -q -O- http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc | apt-key add -
#echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee /etc/apt/sources.list.d/virtualbox.list
#wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
#wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add - #Typora
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAE #Typora
#add-apt-repository -y ppa:mkusb/ppa
#add-apt-repository -y ppa:kritalime/ppa
#add-apt-repository -y ppa:ehbello/fritzing
#add-apt-repository -y ppa:openscad/releases
#add-apt-repository -y ppa:makehuman-official/makehuman-11x
#add-apt-repository -y ppa:freecad-maintainers/freecad-stable
#add-apt-repository -y ppa:vincent-vandevyvre/vvv #QArte
#add-apt-repository -y 'deb http://typora.io ./' #Typora pour Ubuntu
#echo -e "\ndeb https://typora.io/linux ./" | sudo tee -a /etc/apt/sources.list #Typora pour Mint
#add-apt-repository -y ppa:lutris-team/lutris
#Wine
#dpkg --add-architecture i386
#wget -nc https://dl.winehq.org/wine-builds/winehq.key
#apt-key add winehq.key
#apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/'
#Razer
#add-apt-repository -y ppa:openrazer/stable
#add-apt-repository -y ppa:polychromatic/stable
#add-apt-repository -y ppa:minetestdevs/stable
#sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
#add-apt-repository ppa:linuxuprising/shutter
#Celestia
#wget -O- https://bintray.com/user/downloadSubjectPublicKey\?username\=bintray | apt-key add -
#echo "deb https://dl.bintray.com/celestia/releases-deb bionic universe" | tee -a /etc/apt/sources.list
#add-apt-repository ppa:stellarium/stellarium-releases


apt update
#apt upgrade -y

#apt purge ubuntu-web-launchers # Suppression lanceur Amazon

#Admin et config
apt install -y build-essential dkms dracut-core efivar
apt install -y sdparm nvme-cli
apt install -y sysfsutils
apt install -y vim emacs
apt install -y terminator screen shellcheck dos2unix
apt install -y sysstat htop iftop nethogs glances
apt install -y etckeeper borgbackup # Solutions de sauvegarde
apt install -y net-tools nmap whois
apt install -y lynis #Audit sécurité
apt install -y gparted smartmontools tree
apt install -y gnome-software #si pas présent
apt install -y ssh sshfs gnupg2
apt install -y dconf-tools
apt install -y grsync
apt install -y filemanager-actions nautilus-actions #Pour Ubuntu avec environnement Gnome seulement
apt install -y hplip hplip-gui
apt install -y curl
apt install -y fslint fdupes
apt install -y unrar-free rar
apt install -y mkusb
apt install -y default-jre icedtea-plugin #Java
apt install -y winehq-stable cabextract winetricks
apt install -y exfat-fuse exfat-utils ntfs-3g
apt install -y snapd meson
apt install -y gcc #si pas présent
apt install -y flatpak gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
apt install -y mc baobab
apt install -y cowsay fortune fortunes-fr
apt install -y xbacklight redshift #si pas présent
apt install -y openrazer-meta polychromatic
apt install -y lm-sensors fancontrol
apt install -y sipcalc ipcalc #Calculateurs d'IP
apt install -y rename #Rename multiple files
apt install -y cockpit #Interface web de gestion de l'ordinateur Linux
apt install -y unaccent detox convmv #Convert file names from one encoding to another
apt install -y caca-utils #Convert pictures to ASCII.

#Confidentialité
apt install -y xdotool keepassxc keepass2 seahorse gnupg2
apt install -y cardpeek

#DevOps tools
apt install -y docker.io docker-compose
apt install -y git git-lfs
gem install gollum
gem install github-markdown
apt install -y virtualbox-6.1 qemu-kvm vagrant
apt install -y mariadb-server sqlite3 sqlitebrowser
snap install nextcloud
apt install -y nextcloud-desktop
apt install -y poedit
#wget https://download3.vmware.com/software/player/file/VMware-Player-15.1.0-13591040.x86_64.bundle && chmod +x VMware-Player-15.1.0-13591040.x86_64.bundle && ./VMware-Player-15.1.0-13591040.x86_64.bundle
apt install -y texlive-full

#Bureautique
apt install -y scribus abiword

#Internet
apt install -y filezilla chromium-browser qbittorrent xchat torbrowser-launcher google-chrome-stable
apt install -y thunderbird # si pas présent
apt install -y lftp #FTP en ligne de commande

#Synology
#wget https://global.download.synology.com/download/Tools/CloudStationDrive/4.3.1-4437/Ubuntu/Installer/x86_64/synology-cloud-station-drive-4437.x86_64.deb && dpkg --install synology-cloud-station-drive-4437.x86_64.deb
#wget https://global.download.synology.com/download/Tools/Assistant/6.2-23733/Ubuntu/x86_64/synology-assistant_6.2-23733_amd64.deb && dpkg -i synology-assistant_6.2-23733_amd64.deb

#Graphisme
apt install -y gimp inkscape rawtherapee darktable #mypaint
apt install -y krita krita-l10n
apt install -y imagemagick #outil en ligne de commande
#wget https://download.xnview.com/XnViewMP-linux-x64.deb && dpkg -i XnViewMP-linux-x64.deb
#apt install -y gwenview gthumb

#3D
apt install -y openscad librecad freecad
apt install -y blender makehuman
#apt install -y libjava3d-java && apt install -y sweethome3d
snap install sweethome3d-homedesign

#Electronique
#wget https://downloads.arduino.cc/arduino-1.8.9-linux64.tar.xz && tar xvf arduino-1.8.9-linux64.tar.xz && ./arduino-1.8.9/install.sh*
#wget http://s4a.cat/downloads/S4A16.deb && dpkg -i S4A16.deb #Scratch for Arduino
apt install -y kicad kicad-doc-fr #kicad-locale-fr
apt install -y fritzing fritzing-parts
apt install -y geda oregano

#Programmation
apt install -y python3 #si pas présent
apt install -y python3-pip idle3 ipython3
apt install -y lua5.3
apt install -y bluefish geany
#wget http://download.processing.org/processing-3.5.3-linux64.tgz && tar xvf processing-*.tgz && mkdir /usr/share/processing && mv processing-* /usr/share/processing #raccourci à rajouter après

#Markdown
apt install -y pandoc
apt install -y typora

#UML
snap install pynsource
apt install -y umbrello

#Audio
apt install -y ardour lmms audacity sound-juicer k3b brasero
apt install -y vlc #si pas présent

#TV
apt install -y freetuxtv
apt install -y qarte

#Ebooks
wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sh /dev/stdin

#Jeux
apt install -y 0ad freeciv extremetuxracer supertuxkart
#wget http://media.steampowered.com/client/installer/steam.deb && dpkg -i steam.deb
apt install -y playonlinux lutris
#flatpak install flathub net.minetest.Minetest
apt install -y minetest

#Captures d'écran
apt install -y kazam
apt install -y shutter

#Outils réseau
apt install -y gns3
#[[ -e Packet\ Tracer\ 7.2.1\ for\ Linux\ 64\ bit.tar.gz ]] && mkdir packettracer-7.2.1 && sudo tar zxvf Packet\ Tracer\ 7.2.1\ for\ Linux\ 64\ bit.tar.gz -C packettracer-7.2.1 #Terminer install après

#Contrôle à distance
#wget https://dl.teamviewer.com/download/linux/version_14x/teamviewer_14.3.4730_amd64.deb && dpkg -i teamviewer_14.3.4730_amd64.deb
#wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.19.325-Linux-x64.deb && dpkg -i VNC-Viewer-6.19.325-Linux-x64.deb
apt install -y remmina
apt install -y kdeconnect indicator-kdeconnect
#Veyon

#PDF
apt install -y okular #si pas présent
#wget https://code-industry.net/public/master-pdf-editor-5.4.33-qt5.amd64.deb && dpkg -i master-pdf-editor-5.4.33-qt5.amd64.deb
apt install -y pdfshuffler
#apt install -y pdfium
#Foxit Reader

#Astronomie
apt install -y stellarium celestia

#Android
apt install -y adp

#Wordpress
gem install wpscan

#Misc
apt install -y anarchism

apt --fix-broken -y install
apt --fix-missing -y install
apt --reconfigure
dpkg --configure -a
apt install -yf
apt -y autoremove

#scratch et s4A, packetracer, freecad, go, wine, zsh, playonlinux, lutris, kodi, vnc/remmina, flathub, ninja, eagle, cargo (rust)
