# My Ubuntu post-install script

**Pour Mint 19.1, Ubuntu 18.04 et Ubuntu 19.04**

- ppa.sh
- main-install.sh
- flatpak.sh
- wget.sh
- fix-problems.sh

## Upgrade :

```
do-release-upgrade
```

## refind

```
apt install refind
refind-install
```

## Terminator dans l'environnement Gnome :

Les différentes méthodes indiquées pour faire de Terminator le terminal par défaut, et notamment pour qu'il soit utilisé lorsqu'on clique droit "ouvrir dans un terminal" ne semblent pas fonctionner.

### Méthode 1 :

    sudo update-alternatives --config x-terminal-emulator

### Méthode 2 :

    sudo apt-get remove gnome-terminal
    sudo ln -s /usr/bin/terminator /usr/bin/gnome-terminal

### Méthode 3 :

    fma-config-tool

### Lien :

[AskUbuntu](https://askubuntu.com/questions/76712/setting-nautilus-open-terminal-to-launch-terminator-rather-than-gnome-terminal)

## Afficher la liste des fenêtres à droite du tableau de bord de Cinnamon :

Via `dconf-tools`, chercher la clé `org > cinnamon > enable-applets` et changer la ligne : `'panel1:center:11:window-list@cinnamon.org',` pour `'panel1:left:11:window-list@cinnamon.org',`

*Nota *: le troisième champ correspond à la place de l'élément sur le panneau, en partant de la gauche.

Exemple :

    ['panel1:right:1:systray@cinnamon.org:0', 'panel1:left:0:menu@cinnamon.org:1', 'panel1:left:1:show-desktop@cinnamon.org:2', 'panel1:right:2:keyboard@cinnamon.org:4', 'panel1:right:3:notifications@cinnamon.org:5', 'panel1:right:4:removable-drives@cinnamon.org:6', 'panel1:right:7:network@cinnamon.org:8', 'panel1:right:8:sound@cinnamon.org:9', 'panel1:right:9:power@cinnamon.org:10', 'panel1:right:10:calendar@cinnamon.org:11', 'panel1:left:3:window-list@cinnamon.org:12', 'panel1:left:2:panel-launchers@cinnamon.org:13']

## Afficher les thumbnails dans Nemo

Edition > Préférences > Visualisation : augmenter la taille maximale des fichiers à visualiser (clic droit pour fermer la fenêtre)

## Installer Firefox Nightly

### Installation partagée entre tous les utilisateurs
```
tar xvf firefox-89.0a1.fr.linux-x86_64.tar.bz2
mv firefox firefox-nightly
sudo mv firefox-nightly /opt
sudo chown -R $USER:$USER /opt/firefox-nightly
```

### Création et ajout de l'icône aux menus
```
cd
vim nightly.desktop
desktop-file-validate nightly.desktop
desktop-file-install --dir=.local/share/applications nightly.desktop
```


### Lien :

[Forum Mint](https://forums.linuxmint.com/viewtopic.php?f=208&t=94842&start=20#p543274)

## Autres environnements :

[Ubuntu Fr](https://doc.ubuntu-fr.org/tutoriel/faire_cohabiter_plusieurs_gestionnaires_de_bureau)

## PacketTracer 7.2.2 sur Ubuntu 19.10 :

Il faut installer une version corrigée de la librairie libpng12-0 :
- [Téléchargement depuis DropBox](https://www.dropbox.com/s/79x3imq73tcqyw4/libpng12-0_1.2.54-1ubuntu1b_amd64.deb?dl=0)
- [Explications sur Ubuntu-Fr](https://forum.ubuntu-fr.org/viewtopic.php?id=2041956)

## Voir aussi :

- [Ubuntu et Mint sur Asus ROG Strix GL702VMK](https://gitlab.com/sakura-lain/ubuntu-mint-asus-rog-gl702vmk)
- [My ZSH config](https://gitlab.com/sakura-lain/my-zsh-config)
- [My laptop config](https://gitlab.com/sakura-lain/my-laptop-config)
