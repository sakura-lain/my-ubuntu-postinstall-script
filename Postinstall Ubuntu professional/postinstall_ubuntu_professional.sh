#!/bin/bash
# For Ubuntu 18.04 LTS

# Necessary to install VirtualBox Additions
#apt install perl make gcc build-essential dkms

# Depositories
wget -qO - https://typora.io/linux/public-key.asc | apt-key add -
add-apt-repository 'deb https://typora.io/linux ./'
add-apt-repository ppa:webupd8team/atom
add-apt-repository ppa:nextcloud-devs/client

apt update

# Packages
apt install -y filezilla keepassxc python3 ipython3 python3-pip idle3 flatpak snap libreoffice vlc terminator zsh chromium-browser pandoc typora cinnamon-core nextcloud-client atom git shutter pdfshuffler gimp krita inkscape ansible gparted calibre htop sysstat tree

# Snap
snap install panwriter

# Deb packages
dpkg -i synology-cloud-station-drive-4450.x86_64.deb
wget -qnc https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb
dpkg -i nordvpn-release_1.0.0_all.deb
dpkg -i master-pdf-editor-5.4.38-qt5.amd64.deb
