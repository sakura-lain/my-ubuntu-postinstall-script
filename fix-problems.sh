#!/bin/bash

apt --fix-broken -y install
apt --fix-missing -y install
apt --reconfigure
dpkg --configure -a
apt install -yf
apt -y autoremove