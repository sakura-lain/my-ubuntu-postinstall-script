#apt purge ubuntu-web-launchers # Suppression lanceur Amazon

#Admin et config
apt install -y build-essential dkms dracut-core efivar
apt install -y sdparm nvme-cli
apt install -y sysfsutils
apt install -y vim emacs
apt install -y terminator screen shellcheck dos2unix
apt install -y sysstat htop iftop nethogs glances
apt install -y etckeeper borgbackup # Solutions de sauvegarde
apt install -y net-tools nmap whois
apt install -y lynis #Audit sécurité
apt install -y gparted smartmontools tree
apt install -y gnome-software #si pas présent
apt install -y ssh sshfs gnupg2
apt install -y dconf-tools
apt install -y grsync
apt install -y filemanager-actions nautilus-actions #Pour Ubuntu avec environnement Gnome seulement
apt install -y hplip hplip-gui
apt install -y curl
apt install -y fslint fdupes
apt install -y unrar-free rar
apt install -y mkusb
apt install -y default-jre icedtea-plugin #Java
apt install -y winehq-stable cabextract winetricks
apt install -y exfat-fuse exfat-utils ntfs-3g
apt install -y snapd meson
apt install -y gcc #si pas présent
apt install -y mc baobab
apt install -y cowsay fortune fortunes-fr
apt install -y xbacklight redshift #si pas présent
apt install -y openrazer-meta polychromatic
apt install -y lm-sensors fancontrol
apt install -y sipcalc ipcalc #Calculateurs d'IP
apt install -y rename #Rename multiple files
apt install -y cockpit #Interface web de gestion de l'ordinateur Linux
apt install -y unaccent detox convmv #Convert file names from one encoding to another
apt install -y caca-utils #Convert pictures to ASCII.

#Confidentialité
apt install -y xdotool keepassxc keepass2 seahorse gnupg2
apt install -y cardpeek

#DevOps tools
apt install -y docker.io docker-compose
apt install -y git git-lfs
gem install gollum
gem install github-markdown
apt install -y virtualbox-6.1 qemu-kvm vagrant
apt install -y mariadb-server sqlite3 sqlitebrowser
snap install nextcloud
apt install -y nextcloud-desktop
apt install -y poedit
apt install -y texlive-full


#Bureautique
apt install -y scribus abiword

#Internet
apt install -y filezilla chromium-browser qbittorrent xchat torbrowser-launcher google-chrome-stable
apt install -y thunderbird # si pas présent
apt install -y lftp #FTP en ligne de commande

#Graphisme
apt install -y gimp inkscape rawtherapee darktable #mypaint
apt install -y krita krita-l10n
apt install -y imagemagick #outil en ligne de commande
#apt install -y gwenview gthumb

#3D
apt install -y openscad librecad freecad
apt install -y blender makehuman
#apt install -y libjava3d-java && apt install -y sweethome3d
snap install sweethome3d-homedesign

#Electronique
apt install -y kicad kicad-doc-fr #kicad-locale-fr
apt install -y fritzing fritzing-parts
apt install -y geda oregano

#Programmation
apt install -y python3 #si pas présent
apt install -y python3-pip idle3 ipython3
apt install -y lua5.3
apt install -y bluefish geany

#Markdown
apt install -y pandoc
apt install -y typora

#UML
snap install pynsource
apt install -y umbrello

#Audio
apt install -y ardour lmms audacity sound-juicer k3b brasero
apt install -y vlc #si pas présent

#TV
apt install -y freetuxtv
apt install -y qarte

#Jeux
apt install -y 0ad freeciv extremetuxracer supertuxkart
apt install -y playonlinux lutris
apt install -y minetest

#Captures d'écran
apt install -y kazam
apt install -y shutter

#Outils réseau
apt install -y gns3

#Contrôle à distance
apt install -y remmina
apt install -y kdeconnect indicator-kdeconnectcase

#PDF
apt install -y okular #si pas présent
apt install -y pdfshuffler

#Astronomie
apt install -y stellarium celestia

#Android
apt install -y adp

#Wordpress audit sécurité
gem install wpscan

#Misc
apt install -y anarchism
